// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core\Collection\ICollector.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "MyCharacter.generated.h"

class AWeapon;

UCLASS()
class PROJECT0_API AMyCharacter : public ACharacter, public ICollector
{
	GENERATED_BODY()

private:
	TObjectPtr<AWeapon> CurrentWeapon;
	
private:
	UFUNCTION(BlueprintCallable, Category="Player")
	void Attack();
	
public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void OnCollect_Implementation(ACollectable* collectable) override;
};
