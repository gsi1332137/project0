// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ObjectsPool.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PROJECT0_API UObjectsPool : public UActorComponent
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere, Category="ObjectsPool")
	TSubclassOf<AActor> ObjectRef;

	TArray<TObjectPtr<AActor>> Objects;

private:

	template< class T >
	T* SpawnObject()
	{
		T* obj = GetWorld()->SpawnActor<T>(ObjectRef, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
		Objects.Add(obj);

		return obj;
	}
	
public:
	// Sets default values for this component's properties
	UObjectsPool();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	template< class T >
	T* GetObject()
	{
		T* objectToUse = nullptr; 
	
		for (auto obj : Objects)
		{
			if(!obj->IsHidden())
				continue;

			objectToUse = Cast<T>(obj);
		}

		if (!objectToUse)
		{
			objectToUse = SpawnObject<T>();
		}

		return objectToUse;
	}
};
