// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Project0/Core/Interaction/Interactable.h"
#include "Collectable.generated.h"

class ICollector;

UCLASS()
class PROJECT0_API ACollectable : public AInteractable
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere)
	int collectableID;

public:
	// Sets default values for this actor's properties
	ACollectable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Interact(UInteractor* interactor) override;
	
	virtual void Collect(TScriptInterface<ICollector> collector);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	int GetID() const;
};
