#pragma once

#include "ICollector.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UCollector : public UInterface
{
	GENERATED_BODY()
};

class ICollector
{
GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="Collection")
	void OnCollect(ACollectable* collectable);
};