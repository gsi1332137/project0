// Fill out your copyright notice in the Description page of Project Settings.

#include "Collectable.h"

#include "Project0/Core/Collection/ICollector.h"
#include "Project0/Core/Interaction/Interactor.h"

// Sets default values
ACollectable::ACollectable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACollectable::BeginPlay()
{
	Super::BeginPlay();
}

void ACollectable::Interact(UInteractor* interactor)
{
	Super::Interact(interactor);

	if (!interactor->GetOwner()->Implements<UCollector>())
		return;

	Collect(TScriptInterface<ICollector>(interactor->GetOwner()));
}

// Called every frame
void ACollectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int ACollectable::GetID() const
{
	return collectableID;
}

void ACollectable::Collect(TScriptInterface<ICollector> collector)
{
	if (!collector)
		return;
	
	collector->Execute_OnCollect(collector.GetObject(), this);
	
	Destroy();
}

