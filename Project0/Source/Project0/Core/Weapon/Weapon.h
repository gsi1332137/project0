// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Project0/Core/Collection/Collectable.h"
#include "Weapon.generated.h"

class UObjectsPool;
class AMyCharacter;
class AProjectile;

UCLASS()
class PROJECT0_API AWeapon : public ACollectable
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere, Category="Weapon")
	float AttackSpeed;

	UPROPERTY(EditAnywhere, Category="Weapon")
	FVector ProjectileSpawnPosition;

	TObjectPtr<UObjectsPool> ObjectsPool;
	TObjectPtr<AMyCharacter> Holder;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	AWeapon();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Attack();
};
