// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class AMyCharacter;
class AWeapon;

UCLASS()
class PROJECT0_API AProjectile : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere, Category="Projectile")
	float Speed;
	UPROPERTY(EditAnywhere, Category="Projectile")
	float Duration;
	UPROPERTY(EditAnywhere, Category="Projectile")
	bool CanHitHolder;
	
	TObjectPtr<AMyCharacter> MyShooter;
	
public:
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Init(AMyCharacter* shooter, FVector startLocation, FRotator startRotation);

	float GetDuration() const
	{
		return Duration;
	}
};
