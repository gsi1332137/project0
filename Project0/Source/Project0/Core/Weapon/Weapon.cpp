// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include "Projectile.h"
#include "Project0/Core/Pooling/ObjectsPool.h"

// Sets default values
AWeapon::AWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	ObjectsPool = Cast<UObjectsPool>(GetComponentByClass(UObjectsPool::StaticClass()));
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeapon::Attack()
{
	if (!ObjectsPool)
	{
		UE_LOG(LogTemp, Warning, TEXT("Weapon objects pool NOT set"));
		return;
	}

	const auto projectile = ObjectsPool->GetObject<AProjectile>();

	if (!projectile)
	{
		UE_LOG(LogTemp, Error, TEXT("Projectile NOT found in pool"));
		return;
	}

	projectile->Init(Holder, ProjectileSpawnPosition, GetActorRotation());
	projectile->SetHidden(false);
}

