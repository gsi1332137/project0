// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Weapon.h"
#include "Project0/MyCharacter.h"

// Sets default values
AProjectile::AProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Speed > 0.f)
	{
		const auto currentLocation = GetActorLocation();
		SetActorLocation(currentLocation + GetActorForwardVector() * Speed * DeltaTime);
	}	
}

void AProjectile::Init(AMyCharacter* shooter, FVector startLocation, FRotator startRotation)
{
	MyShooter = shooter;
	SetActorLocation(startLocation);
	SetActorRotation(startRotation);
}
