// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interactor.generated.h"

class AInteractable;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECT0_API UInteractor : public UActorComponent
{
	GENERATED_BODY()

private:
	TObjectPtr<AActor> MyActor;
	TObjectPtr<AInteractable> InteractableInFocus;

private:
	AInteractable* GetInteractableInFocus() const;
	void SetInteractableInFocus(AInteractable* interactableToSet);

	UFUNCTION(BlueprintCallable, Category="Collector")
	void Interact();
	UFUNCTION(BlueprintCallable, Category="Collector")
	void Update();
	
public:	
	// Sets default values for this component's properties
	UInteractor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
};
