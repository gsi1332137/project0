// Fill out your copyright notice in the Description page of Project Settings.

#include "Interactable.h"

// Sets default values
AInteractable::AInteractable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();

	MyMesh = GetComponentByClass<UStaticMeshComponent>();
	isHighlighted = false;
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AInteractable::Highlight(bool highlight)
{
	if (!MyMesh)
		return;

	isHighlighted = highlight;
	MyMesh->SetMaterial(0, isHighlighted ? HighlightedMaterial : DefaultMaterial);
}

void AInteractable::Interact(UInteractor* interactor)
{
	UE_LOG(LogTemp, Display, TEXT("Interacted with %s"), *GetName());
}

