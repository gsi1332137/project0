// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactor.h"
#include "Interactable.h"

// Sets default values for this component's properties
UInteractor::UInteractor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UInteractor::BeginPlay()
{
	Super::BeginPlay();

	MyActor = GetOwner();	
}

void UInteractor::Update()
{
	const auto newCollectableInFocus = GetInteractableInFocus();
	
	if (!InteractableInFocus)
	{
		SetInteractableInFocus(newCollectableInFocus);
	}	
	else if (newCollectableInFocus != InteractableInFocus)
	{
		InteractableInFocus->Highlight(false);
		SetInteractableInFocus(newCollectableInFocus);
	}
}

AInteractable* UInteractor::GetInteractableInFocus() const
{
	if (!MyActor)
		return nullptr;
	
	TArray<AActor*> interactablesInRange;
	MyActor->GetOverlappingActors(interactablesInRange, AInteractable::StaticClass());
	FVector myActorLocation = MyActor->GetActorLocation();
	float minItemAngle = std::numeric_limits<float>::max();
	AActor* actorInFocus = nullptr;

	for (auto interactable : interactablesInRange)
	{
		FVector interactableLocation = interactable->GetActorLocation();
		FVector interactableDirection = (interactableLocation - myActorLocation).GetSafeNormal();
		float dotProduct = FVector::DotProduct(interactableDirection, MyActor->GetActorRotation().Vector().GetSafeNormal());

		float angle = FMath::RadiansToDegrees(acosf(dotProduct));
		if (angle < minItemAngle)
		{
			minItemAngle = angle;
			actorInFocus = interactable;
		}
	}
	
	if (!actorInFocus)
		return nullptr;

	return Cast<AInteractable>(actorInFocus);
}

void UInteractor::SetInteractableInFocus(AInteractable* interactableToSet)
{
	InteractableInFocus = interactableToSet;

	if (InteractableInFocus)
	{
		InteractableInFocus->Highlight(true);
	}
}

void UInteractor::Interact()
{
	if (!InteractableInFocus)
		return;

	InteractableInFocus->Interact(this);
}
