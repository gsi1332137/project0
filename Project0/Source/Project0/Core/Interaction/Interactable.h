// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

class UInteractor;

UCLASS()
class PROJECT0_API AInteractable : public AActor
{
	GENERATED_BODY()

protected:
	bool isHighlighted;

	TObjectPtr<UStaticMeshComponent> MyMesh;
	
	UPROPERTY(EditAnywhere)
	TObjectPtr<UMaterial> DefaultMaterial;
	
	UPROPERTY(EditAnywhere)
	TObjectPtr<UMaterial> HighlightedMaterial;

public:
	// Sets default values for this actor's properties
	AInteractable();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Highlight(bool highlight);
	virtual void Interact(UInteractor* interactor);
};
