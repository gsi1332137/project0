// Fill out your copyright notice in the Description page of Project Settings.

#include "MyCharacter.h"

#include "Core/Collection/Collectable.h"
#include "Core/Weapon/Weapon.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMyCharacter::OnCollect_Implementation(ACollectable* collectable)
{
	UE_LOG(LogTemp, Display, TEXT("Applying collectable effect to %s"), *GetName());

	if (!collectable->IsA(AWeapon::StaticClass()))
		return;

	CurrentWeapon = Cast<AWeapon>(collectable);
}

void AMyCharacter::Attack()
{
	if (!CurrentWeapon)
		return;

	CurrentWeapon->Attack();
}